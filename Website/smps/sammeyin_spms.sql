-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 22, 2018 at 12:55 AM
-- Server version: 5.6.39
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sammeyin_spms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `secondName` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` char(36) NOT NULL,
  `email` varchar(45) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `firstName`, `secondName`, `username`, `password`, `email`, `last_login`) VALUES
(18, 'Sam', 'S', 'sam', '123456', 'sam@gmail.com', '2018-09-22 01:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `all_students`
--

CREATE TABLE `all_students` (
  `id` int(11) DEFAULT NULL,
  `firstName` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group` char(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `all_students`
--

INSERT INTO `all_students` (`id`, `firstName`, `lastName`, `module`, `email`, `group`) VALUES
(6, 'Simran', 'M', 'CS102', 'simran@gmail.com', 'B'),
(2, 'Aditya', 'Gosalia', 'CS101', 'aditya@gmail.com', 'A'),
(1, 'Sagar', 'Mistry', 'CS101', 'sagar@gmail.com', 'A'),
(3, 'Dhairya', 'Bhatt', 'CS101', 'dhairya@gmail.com', 'A'),
(4, 'Pooja', 'I', 'CS101', 'pooja@gmail.com', 'A'),
(5, 'Dhruvisha', 'G', 'CS101', 'dhruvisha@gmail.com', 'A'),
(7, 'Sameer', 'M', 'CS102', 'sameer@gmail.com', 'B'),
(8, 'Rujuta', 'M', 'CS102', 'rujuta@gmail.com', 'B'),
(9, 'Aarefa', 'G', 'CS102', 'aarefa@gmail.com', 'B'),
(10, 'Ram', 'S', 'CS102', 'ram@gmail.com', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `lab`
--

CREATE TABLE `lab` (
  `id` int(11) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `finish_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lab`
--

INSERT INTO `lab` (`id`, `start_date`, `finish_date`) VALUES
(1, '2013-05-17 17:12:35', '2013-05-24 17:12:35'),
(2, '2013-05-24 12:33:48', '2013-05-31 17:12:35'),
(3, '2013-05-31 12:33:14', '2013-06-07 16:43:36');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` bigint(20) NOT NULL,
  `student_id` char(36) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `mark` int(3) NOT NULL,
  `colour` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `student_id`, `lab_id`, `mark`, `colour`) VALUES
(1, '1', 1, 50, 'green'),
(2, '2', 1, 40, 'orange'),
(3, '1', 2, 70, 'green'),
(4, '2', 2, 10, 'red'),
(5, '3', 3, 70, 'green'),
(6, '1', 3, 90, 'orange'),
(7, '3', 2, 40, 'orange'),
(12, '2', 3, 70, 'green'),
(13, '4', 1, 50, 'orange'),
(14, '4', 3, 50, 'orange'),
(15, '5', 2, 30, 'red'),
(16, '5', 3, 30, 'red'),
(17, '6', 1, 70, 'green'),
(18, '6', 2, 50, 'orange'),
(19, '6', 3, 30, 'red'),
(22, '7', 1, 30, 'red'),
(23, '7', 2, 50, 'orange'),
(24, '7', 3, 75, 'green'),
(25, '8', 1, 70, 'green'),
(26, '8', 2, 70, 'green'),
(27, '8', 3, 70, 'green'),
(28, '9', 1, 50, 'orange'),
(29, '9', 2, 50, 'orange'),
(30, '10', 1, 30, 'red'),
(31, '10', 2, 30, 'red'),
(32, '10', 3, 30, 'red');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `module` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `Group` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `firstName`, `lastName`, `module`, `email`, `Group`) VALUES
(1, 'James', 'King', 'CS101', 'jking@fakemail.com', 'A'),
(2, 'Julie', 'Taylor', 'CS101', 'jtaylor@fakemail.com', 'A'),
(3, 'John', 'Williams', 'CS101', 'jwilliams@fakemail.com', 'A'),
(4, 'Eugene', 'Lee', 'CS101', 'elee@fakemail.com', 'A'),
(5, 'Paul', 'Jones', 'CS102', 'pjones@fakemail.com', 'A'),
(6, 'Ray', 'Moore', 'CS102', 'rmooe@fakemail.com', 'B'),
(7, 'Paula', 'Gates', 'CS102', 'pgates@fakemail.com', 'B'),
(8, 'Lisa', 'Wong', 'CS102', 'lwong@fakemail.com', 'B'),
(9, 'Gary', 'Donovan', 'CS101', 'gdonovan@fakemail.com', 'B'),
(10, 'John', 'Byrne', 'CS101', 'jbyrne@fakemail.com', 'B');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `username_2` (`username`);

--
-- Indexes for table `lab`
--
ALTER TABLE `lab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`,`lab_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
